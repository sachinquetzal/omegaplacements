<?php
  // src/Controller/SearchController
  namespace App\Controller;

  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Routing\Annotation\Route;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;

  class SearchController extends controller {
    /**
    *@Route("/search/{name}")
    **/
    public function show($name) {
      return $this->render('search/search.html.twig', array(
        'name' => $name,
      ));
    }
  }
 ?>
